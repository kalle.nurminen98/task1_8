## Table of Contents

- [Sections](#sections)
  - [Title](#title)
  - [Short Description](#short-description)
  - [Install](#install)
  - [Usage](#usage)
  - [Maintainers](#maintainers)
  - [Contributing](#contributing)
  - [License](#license)

## Sections

### Title
RPG Character Generator 

### Short Description

Task was to make various custom classes that are written in OO style. I didn't get my files to compile but if I put everything to one file (result.cpp) then it worked.

### Install
Make sure you have gcc version 11.1.0  
g++ main.cpp -o main and cmake

### Usage
$ mkdir build && cd build  
$ cmake ../  
$ make  
$ ./build

### Maintainer(s)

Kalle Nurminen @kalle.nurminen98

### Contributing

Kalle Nurminen @kalle.nurminen98

### License

Free to use. No license