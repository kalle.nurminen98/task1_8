#include <iostream>
#include <list>

class Character
{
    public:
    virtual void set_level()
    {};
    virtual void set_focus()
    {};
    virtual void set_health()
    {};
    virtual void set_body()
    {};
    virtual void set_speed()
    {};
    virtual void set_knowledge()
    {};
    virtual void set_melee_damage()
    {};
    virtual void set_ranged_damage()
    {};
    virtual void set_spell_damage()
    {};
};

class Equipment
{
    public:
    virtual void set_more_body()
    {};
    virtual void set_more_speed()
    {};
    virtual void set_more_knowledge()
    {};
    virtual void set_more_melee_damage()
    {};
};
class Weapons : public Equipment
{
    public:
    int dagger = 20;
    int staff = 5;
    int sword = 30;
    void set_more_melee_damage()
    {
        int dagger;
        int staff;
        int sword;
    }
};
class Armour : public Equipment
{
    public:
    int robe = 5;
    int plate_mail = 30;
    int leather_armour = 10;
    void set_more_body()
    {
        int robe;
        int plate_mail;
        int leather_armour;
    }
};

class Accessories : public Equipment
{
    public:
    int speed_potion = 10;
    int knowledge_potion = 10;
    void set_more_speed()
    {
        int speed_potion;
    }
    void set_more_knowledge()
    {
        int knowledge_potion;
    }
};

class Warrior : public Character, public Weapons, public Armour
{
    public:
    int health = 100;
    int body = 70;
    int speed = 30;
    int knowledge = 20;
    int melee_damage = 50;
    int ranged_damage = 0;
    int spell_damage = 0;
    int level = 2;
    int level_two_multiplier = 2;
    void set_level()
    {
        if(level == 2)
        {
            health = health * level_two_multiplier;
            body = body * level_two_multiplier;
            melee_damage = melee_damage * level_two_multiplier;
        }
    }
    void set_focus()
    {
        std::cout << "Warrior, melee ";
    }
    void set_health()
    {
        std::cout << health << " ";
    }
    void set_body()
    {
        body = body + plate_mail;
        std::cout << body << " ";
    }
    void set_speed()
    {
        std::cout << speed << " ";
    }
    void set_knowledge()
    {
        std::cout << knowledge << " ";
    }
    void set_melee_damage()
    {
        melee_damage = melee_damage + sword;
        std::cout << melee_damage << " ";
    }
    void set_ranged_damage()
    {
        std::cout << ranged_damage << " ";
    }
    void set_spell_damage()
    {
        std::cout << spell_damage << " " << std::endl;
    }
};

class Mage : public Character, public Accessories
{
    public:
    int health = 100;
    int body = 15;
    int speed = 40;
    int knowledge = 80;
    int melee_damage = 5;
    int ranged_damage = 0;
    int spell_damage = 60;
    int level = 1;
    int level_two_multiplier = 2;
    void set_level()
    {
        if(level == 2)
        {
            health = health * level_two_multiplier;
            knowledge = knowledge * level_two_multiplier;
            spell_damage = spell_damage * level_two_multiplier;
        }
    }
    void set_focus()
    {
        std::cout << "Mage, magic ";
    }
    void set_health()
    {
        std::cout << health << " ";
    }
    void set_body()
    {
        std::cout << body << " ";
    }
    void set_speed()
    {
        std::cout << speed << " ";
    }
    void set_knowledge()
    {
        set_more_knowledge();
        knowledge = knowledge + knowledge_potion;
        std::cout << knowledge << " ";
    }
    void set_melee_damage()
    {
        std::cout << melee_damage << " ";
    }
    void set_ranged_damage()
    {
        std::cout << ranged_damage << " ";
    }
    void set_spell_damage()
    {
        std::cout << spell_damage << " " << std::endl;
    }
};

class Rogue : public Character
{
    public:
    int health = 100;
    int body = 5;
    int speed = 75;
    int knowledge = 75;
    int melee_damage = 70;
    int ranged_damage = 0;
    int spell_damage = 0;
    int level = 2;
    int level_two_multiplier = 2;
    void set_level()
    {
        if(level == 2)
        {
            melee_damage = melee_damage * level_two_multiplier;
            speed = speed * level_two_multiplier;
            knowledge = knowledge * level_two_multiplier;
        }
    }
    void set_focus()
    {
        std::cout << "Rogue, stealth ";
    }
    void set_health()
    {
        std::cout << health << " ";
    }
    void set_body()
    {
        std::cout << body << " ";
    }
    void set_speed()
    {
        std::cout << speed << " ";
    }
    void set_knowledge()
    {
        std::cout << knowledge << " ";
    }
    void set_melee_damage()
    {
        std::cout << melee_damage << " ";
    }
    void set_ranged_damage()
    {
        std::cout << ranged_damage << " ";
    }
    void set_spell_damage()
    {
        std::cout << spell_damage << " " << std::endl;
    }
};

class Ranger : public Character
{
    public:
    int health = 100;
    int body = 10;
    int speed = 70;
    int knowledge = 40;
    int melee_damage = 10;
    int ranged_damage = 55;
    int spell_damage = 0;
    int level = 1;
    int level_two_multiplier = 2;
    void set_level()
    {
        if(level == 2)
        {
            speed = speed * level_two_multiplier;
            ranged_damage = ranged_damage * level_two_multiplier;
        }
    }
    void set_focus()
    {
        std::cout << "ranger, range ";
    }
    void set_health()
    {
        std::cout << health << " ";
    }
    void set_body()
    {
        std::cout << body << " ";
    }
    void set_speed()
    {
        std::cout << speed << " ";
    }
    void set_knowledge()
    {
        std::cout << knowledge << " ";
    }
    void set_melee_damage()
    {
        std::cout << melee_damage << " ";
    }
    void set_ranged_damage()
    {
        std::cout << ranged_damage << " ";
    }
    void set_spell_damage()
    {
        std::cout << spell_damage << " " << std::endl;
    }
};

int main()
{
    std::list<Character*> characters;

    characters.push_back(new Warrior());
    characters.push_back(new Mage());
    characters.push_back(new Rogue());
    characters.push_back(new Ranger());
    std::cout << "Type, focus, health, body, speed, knowledge, melee_damage, ranged_damage, spell_damage" << std::endl;
    for(auto cha : characters)
    {
        cha -> set_level();
        cha -> set_focus();
        cha -> set_health();
        cha -> set_body();
        cha -> set_speed();
        cha -> set_knowledge();
        cha -> set_melee_damage();
        cha -> set_ranged_damage();
        cha -> set_spell_damage();
    }
    return 0;
}