#include <iostream>

class Equipment
{
    public:
    virtual void set_more_body()
    {};
    virtual void set_more_speed()
    {};
    virtual void set_more_knowledge()
    {};
    virtual void set_more_melee_damage()
    {};
};
class Weapons : public Equipment
{
    public:
    virtual void set_more_melee_damage()
    {};
};
class Armour : public Equipment
{
    public:
    virtual void set_more_body()
    {};
};
class Accessories : public Equipment
{
    public:
    virtual void set_more_speed()
    {};
    virtual void set_more_knowledge()
    {};
};