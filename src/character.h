#include <iostream>

class Character
{
    public:
    virtual void set_level()
    {};
    virtual void set_focus()
    {};
    virtual void set_health()
    {};
    virtual void set_body()
    {};
    virtual void set_speed()
    {};
    virtual void set_knowledge()
    {};
    virtual void set_melee_damage()
    {};
    virtual void set_ranged_damage()
    {};
    virtual void set_spell_damage()
    {};
};
class Warrior : public Character
{
    public:
    virtual void set_level()
    {};
    virtual void set_focus()
    {};
    virtual void set_health()
    {};
    virtual void set_body()
    {};
    virtual void set_speed()
    {};
    virtual void set_knowledge()
    {};
    virtual void set_melee_damage()
    {};
    virtual void set_ranged_damage()
    {};
    virtual void set_spell_damage()
    {};
};

class Mage : public Character
{
    public:
    virtual void set_level()
    {};
    virtual void set_focus()
    {};
    virtual void set_health()
    {};
    virtual void set_body()
    {};
    virtual void set_speed()
    {};
    virtual void set_knowledge()
    {};
    virtual void set_melee_damage()
    {};
    virtual void set_ranged_damage()
    {};
    virtual void set_spell_damage()
    {};
};

class Rogue : public Character
{
    public:
    virtual void set_level()
    {};
    virtual void set_focus()
    {};
    virtual void set_health()
    {};
    virtual void set_body()
    {};
    virtual void set_speed()
    {};
    virtual void set_knowledge()
    {};
    virtual void set_melee_damage()
    {};
    virtual void set_ranged_damage()
    {};
    virtual void set_spell_damage()
    {};
};

class Ranger : public Character
{
    public:
    virtual void set_level()
    {};
    virtual void set_focus()
    {};
    virtual void set_health()
    {};
    virtual void set_body()
    {};
    virtual void set_speed()
    {};
    virtual void set_knowledge()
    {};
    virtual void set_melee_damage()
    {};
    virtual void set_ranged_damage()
    {};
    virtual void set_spell_damage()
    {};
};