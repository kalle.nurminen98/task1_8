#include <iostream>
#include <list>
#include "equipment.h"
#include "character.h"

int main()
{
    std::list<Character*> characters;

    characters.push_back(new Warrior());
    characters.push_back(new Mage());
    characters.push_back(new Rogue());
    characters.push_back(new Ranger());
    std::cout << "Type, focus, health, body, speed, knowledge, melee_damage, ranged_damage, spell_damage" << std::endl;
    for(auto cha : characters)
    {
        cha -> set_level();
        cha -> set_focus();
        cha -> set_health();
        cha -> set_body();
        cha -> set_speed();
        cha -> set_knowledge();
        cha -> set_melee_damage();
        cha -> set_ranged_damage();
        cha -> set_spell_damage();
    }
    return 0;
}